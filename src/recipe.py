from abc import ABC

from cup import Cup


class Recipe(ABC):

    def make(self) -> Cup: pass
