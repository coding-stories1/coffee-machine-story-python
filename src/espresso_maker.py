from abstract_maker import AbstractMaker
from cup import Cup


class EspressoMaker(AbstractMaker):

    def make(self):
        cup: Cup = self._take_new_empty_cup()
        cup.add("espresso")
        return cup
