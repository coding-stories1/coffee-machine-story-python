import unittest

from americano_maker import AmericanoMaker
from cappuccino_maker import CappuccinoMaker
from coffee_machine import CoffeeMachine
from espresso_maker import EspressoMaker
from latte_maker import LatteMaker
from mocaccino_maker import MocaccinoMaker


class CoffeeMachineTestCase(unittest.TestCase):
    
    coffee_machine = CoffeeMachine()

    def setUp(self) -> None:
        makers = {
            "AMERICANO": AmericanoMaker(),
            "ESPRESSO": EspressoMaker(),
            "LATTE": LatteMaker(),
            "CAPPUCCINO": CappuccinoMaker(),
            "MOCACCINO": MocaccinoMaker(),
        }
        self.coffee_machine.set_makers(makers)

    def test_get_americano_recipe(self):
        response = self.coffee_machine.process_request("AMERICANO")
        expected_response = {
            "espresso": 1,
            "water": 1,
            "milk": 0,
            "foam": 0,
        }
        self.assertEqual(expected_response, response)

    def test_get_espresso_recipe(self):
        response = self.coffee_machine.process_request("ESPRESSO")
        expected_response = {
            "espresso": 1,
            "water": 0,
            "milk": 0,
            "foam": 0,
        }
        self.assertEqual(expected_response, response)

    def test_get_latte_recipe(self):
        response = self.coffee_machine.process_request("LATTE")
        expected_response = {
            "espresso": 1,
            "water": 0,
            "milk": 2,
            "foam": 1,
        }
        self.assertEqual(expected_response, response)

    def test_throw_exception_when_recipe_is_unknown(self):
        with self.assertRaises(RuntimeError):
            self.coffee_machine.process_request("TEA")

    def test_get_cappuccino_recipe(self):
        response = self.coffee_machine.process_request("CAPPUCCINO")
        expected_response = {
            "espresso": 1,
            "water": 0,
            "milk": 1,
            "foam": 1,
        }
        self.assertEqual(expected_response, response)

    def test_get_mocaccino_recipe(self):
        response = self.coffee_machine.process_request("MOCACCINO")
        expected_response = {
            "espresso": 1,
            "water": 0,
            "milk": 2,
            "foam": 1,
            "chocolate": 1,
        }
        self.assertEqual(expected_response, response)
