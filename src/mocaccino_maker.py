from abstract_maker import AbstractMaker
from cup import Cup


class MocaccinoMaker(AbstractMaker):

    def make(self) -> Cup:
        cup: Cup = Cup()
        cup.add("espresso")
        cup.add("milk")
        cup.add("milk")
        cup.add("foam")
        cup.add("chocolate")
        return cup