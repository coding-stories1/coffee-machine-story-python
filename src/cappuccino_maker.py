from abstract_maker import AbstractMaker
from cup import Cup


class CappuccinoMaker(AbstractMaker):

    def make(self):
        cup: Cup = self._take_new_empty_cup()
        cup.add("espresso")
        cup.add("milk")
        cup.add("foam")
        return cup
