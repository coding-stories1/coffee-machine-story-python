from typing import Dict

from recipe import Recipe


class CoffeeMachine:
    __makers: Dict[str, Recipe] = {}

    def process_request(self, request: str) -> dict:
        if request in self.__makers:
            return self.__makers[request].make().compose_response()
        else:
            raise RuntimeError("Wrong Request")

    def get_makers(self):
        return self.__makers

    def set_makers(self, makers: Dict[str, Recipe]):
        self.__makers = makers
