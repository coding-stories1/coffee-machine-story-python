# Coffee Machine Story

**To read the story**: https://codingstories.io/stories/6139f4a2f2cd3d0031cd8f91/6192d49c7e944b001d794406

**To code yourself**: https://gitlab.com/coding-stories1/coffee-machine-story-python

**Estimated reading time**: 45 minutes

## Story Outline
Read a small story about Coffee Machine Controller which is used to get right ingredients for different coffee drinks.

This story is about violations of clean design principles with focus on
Open-Closed Principle (O in SOLID).

## Story Organization
**Story Branch**: master
> `git checkout master`

**Practical task tag for self-study**: task
> `git checkout task`

Tags: #clean_design, #python, #open_closed_principle
